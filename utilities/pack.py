import ctypes
import struct

from c2w.main.constants import ROOM_IDS


# function to create packets (loginRequest, message, joinRoomRequest)
def createPacket(frg, ack, Type, rt, sequenceNumber, userId, destinationId, dataLength, data=None):
    # creating the first byte - shifting
    firstByte = (frg << 7) | (ack << 6) | (Type << 2) | (rt)
    buf = ctypes.create_string_buffer(6 + dataLength)

    if (data != None):
        struct.pack_into('>BBBBH' + str(dataLength) + 's', buf, 0, firstByte, sequenceNumber, userId, destinationId,
                         dataLength, data)
    else:
        struct.pack_into('>BBBBH', buf, 0, firstByte, sequenceNumber, userId, destinationId, dataLength)
    return buf


# function used to unpack the packets
def unpackPacket(datagram):
    dataLength = len(datagram) - 6
    packet = struct.unpack_from('>BBBBH' + str(dataLength) + 's', datagram)
    firstByte = packet[0]
    sequenceNumber = packet[1]
    userId = packet[2]
    destinationId = packet[3]
    dataLength = packet[4]
    data = packet[5]
    return (firstByte, sequenceNumber, userId, destinationId, dataLength, data)


# function to create acknowledgements
def createAck(Type, sequenceNumber, userId, destinationId):
    # defining fields of the first byte
    frg = 0b0
    ack = 0b1
    rt = 0b11
    firstByte = (frg << 7) | (ack << 6) | (Type << 2) | (rt)  # creating the first byte - shifting
    buf = ctypes.create_string_buffer(6)
    struct.pack_into('>BBBBH', buf, 0, firstByte, sequenceNumber, userId, destinationId, 0)
    return buf


# function to create an acknowledgement for joinRoomRequest (with address)
def createRoomRequestAck(Type, sequenceNumber, userId, destinationId, dataLength, ip=None, port=None):
    # defining fields of the first byte
    frg = 0b0
    ack = 0b1
    rt = 0b11

    # creating the first byte - shifting
    firstByte = (frg << 7) | (ack << 6) | (Type << 2) | (rt)
    buf = ctypes.create_string_buffer(12)

    ips = []

    if (ip != None):

        for counter in range(1, 4):
            pointIndex = ip.index('.')
            ips.append(int(ip[0:pointIndex]))
            ip = ip[pointIndex + 1:]

        ips.append(int(ip))
        struct.pack_into('>BBBBHBBBBH', buf, 0, firstByte, sequenceNumber, userId, destinationId, dataLength, ips[0],
                         ips[1], ips[2], ips[3], port)

    else:
        struct.pack_into('>BBBBH', buf, 0, firstByte, sequenceNumber, userId, destinationId, dataLength)

    return buf


# function used to unpack the packets
def unpackRoomRequestAck(datagram):
    ips = []
    for j in range(6, 10):
        x = struct.unpack_from('B', datagram, j)
        ips.append(x[0])

    ip = str(ips[0]) + '.' + str(ips[1]) + '.' + str(ips[2]) + '.' + str(ips[3])
    y = struct.unpack_from('!H', datagram, 10)
    port = y[0]

    return (ip, port)


# function used to pack the movieList
def packMovieList(movieList, userId, seqNumber):
    # defining fields of the first byte
    # frg = 0b0
    # ack = 0b0
    # Type = 0b0011
    # rt = 0b11
    firstByte = 0b00001111
    sequenceNumber = seqNumber
    destinationId = 0b00000000
    dataLength = 0

    for i in range(len(movieList)):
        lenMovieTitle = len(movieList[i].movieTitle)
        dataLength += lenMovieTitle + 2

    buf = ctypes.create_string_buffer(dataLength + 6)
    struct.pack_into('>BBBBH', buf, 0, firstByte, sequenceNumber, userId, destinationId, dataLength)
    currentPosition = 6

    for i in range(len(movieList)):
        lenMovieTitle = len(movieList[i].movieTitle)
        struct.pack_into('>B', buf, currentPosition, lenMovieTitle)
        struct.pack_into('>B', buf, currentPosition + 1, movieList[i].movieId)
        struct.pack_into('>' + str(lenMovieTitle) + 's', buf, currentPosition + 2, movieList[i].movieTitle)
        currentPosition = currentPosition + 2 + lenMovieTitle

    return buf


# function used to unpack the movieList
def unpackMovieList(datagram):
    dataLength = struct.unpack_from('>H', datagram, 4)
    currentPosition = 6
    movieList = []
    movieByName = {}

    while (currentPosition < dataLength[0] + 6):
        length = struct.unpack_from('>B', datagram, currentPosition)
        Id = struct.unpack_from('>B', datagram, currentPosition + 1)
        title = struct.unpack_from(str(length[0]) + 's', datagram, currentPosition + 2)
        currentPosition = currentPosition + length[0] + 2
        movieList.append((title[0], 0, 0))
        movieByName[title[0]] = Id[0]

    return (movieList, movieByName)


# function used to pack the userList
def packUserList(userList, userId, seqNumber):
    # defining fields of the first byte
    # frg = 0b0  
    # ack = 0b0
    # Type = 0b0101
    # rt = 0b00
    firstByte = 0b00010100
    sequenceNumber = seqNumber
    destinationId = 0b00000000
    dataLength = 0

    for i in range(len(userList)):
        lenUserName = len(userList[i].userName)
        dataLength += lenUserName + 3

    buf = ctypes.create_string_buffer(dataLength + 6)
    struct.pack_into('>BBBBH', buf, 0, firstByte, sequenceNumber, userId, destinationId, dataLength)
    currentPosition = 6

    for i in range(len(userList)):

        lenUserName = len(userList[i].userName)
        struct.pack_into('>B', buf, currentPosition, lenUserName)
        struct.pack_into('>B', buf, currentPosition + 1, userList[i].userId)

        if (userList[i].userChatRoom == ROOM_IDS.MAIN_ROOM):
            struct.pack_into('>B', buf, currentPosition + 2, 0b10000000)
        else:
            struct.pack_into('>B', buf, currentPosition + 2, 0b00000000)

        struct.pack_into('>' + str(lenUserName) + 's', buf, currentPosition + 3, userList[i].userName)
        currentPosition += 3 + lenUserName

    return buf


# function used to unpack the userList
def unpackUserList(datagram, movieName):
    dataLength = struct.unpack_from('>H', datagram, 4)
    currentPosition = 6
    userList = []
    userById = {}

    while (currentPosition < dataLength[0] + 6):
        length = struct.unpack_from('>B', datagram, currentPosition)
        iD = struct.unpack_from('>B', datagram, currentPosition + 1)
        status = struct.unpack_from('>B', datagram, currentPosition + 2)
        userName = struct.unpack_from(str(length[0]) + 's', datagram, currentPosition + 3)

        if (status[0] == 0b00000000):

            if (movieName == ROOM_IDS.MAIN_ROOM):
                room = ROOM_IDS.MOVIE_ROOM
            else:
                room = movieName

        elif (status[0] == 0b10000000):
            room = ROOM_IDS.MAIN_ROOM

        userList.append((userName[0], room))
        userById[iD[0]] = userName[0]
        currentPosition = currentPosition + length[0] + 3

    return userList, userById
