# -*- coding: utf-8 -*-
import logging
import struct

from c2w.main.constants import ROOM_IDS
from twisted.internet.protocol import Protocol
from utilities import pack

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.tcp_chat_client_protocol')


class c2wTcpChatClientProtocol(Protocol):
    def __init__(self, clientProxy, serverAddress, serverPort):
        """
        :param clientProxy: The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.
        :param serverAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param serverPort: The port number used by the c2w server,
            given by the user.

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: clientProxy

            The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        .. attribute:: serverAddress

            The IP address (or the name) of the c2w server.

        .. attribute:: serverPort

            The port number used by the c2w server.

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """
        self.serverAddress = serverAddress
        self.serverPort = serverPort
        self.clientProxy = clientProxy

        self.movieList = []
        self.userList = []

        self.movieName = ROOM_IDS.MAIN_ROOM

        self.userId = 0  # the ID of the user
        self.cssn = 0  # client side sequence number stored by the client
        self.sssn = 0  # server side sequence number stored by the client
        self.initialization = 0  # variable which assures that initCompleteONE function is called only once

        self.movieByName = {}  # dictionary linking the movieId to its title - title is a key
        self.userById = {}  # dictionary linking the userName to his id - id is a key

        self.buf = None

    def sendLoginRequestOIE(self, userName):
        """
        :param string userName: The user name that the user has typed.

        The controller calls this function as soon as the user clicks on
        the login button.
        """

        # defining fields of the packet
        frg = 0b0
        ack = 0b0
        type = 0b0000
        rt = 0b11
        sequenceNumber = 0b00000000
        dataLength = len(userName)
        userId = 0b00000000
        destinationId = 0b00000000

        # creating the loginRequest packet
        buf = pack.createPacket(frg, ack, type, rt, sequenceNumber, userId, destinationId, dataLength, userName)
        self.transport.write(buf.raw)

    def sendChatMessageOIE(self, message):
        """
        :param message: The text of the chat message.
        :type message: string
        Called **by the controller**  when the user has decided to send
        a chat message

        .. note::
           This is the only function handling chat messages, irrespective
           of the room where the user is.  Therefore it is up to the
           c2wChatClientProctocol or to the server to make sure that this
           message is handled properly, i.e., it is shown only by the
           client(s) who are in the same room.
        """

        # defining fields of the packet
        frg = 0b0
        ack = 0b0
        typeField = 0b0001
        sequenceNumber = self.cssn
        dataLength = len(message)

        if (self.movieName == ROOM_IDS.MAIN_ROOM):  # distinguishing between movie room and main room
            destinationId = 0
            rt = 0b00
        else:
            destinationId = self.movieByName[self.movieName]
            rt = 0b01

        buf = pack.createPacket(frg, ack, typeField, rt, sequenceNumber, self.userId, destinationId, dataLength,
                                message)  # creating the chatMessage packet
        self.transport.write(buf.raw)

    def sendJoinRoomRequestOIE(self, roomName):
        """
        :param roomName: The room name (or movie title.)

        Called **by the controller**  when the user
        has clicked on the watch button or the leave button,
        indicating that she/he wants to change room.

        .. warning:
            The controller sets roomName to
            c2w.main.constants.ROOM_IDS.MAIN_ROOM when the user
            wants to go back to the main room.
        """

        # defining fields of the packet
        frg = 0b0
        ack = 0b0
        typeField = 0b1000
        rt = 0b01
        self.movieName = roomName
        sequenceNumber = self.cssn
        dataLength = 0
        userId = self.userId

        # distinguishing between join movie room and leave movie room
        if (roomName == ROOM_IDS.MAIN_ROOM):
            destinationId = 0
        else:
            destinationId = self.movieByName[roomName]

        buf = pack.createPacket(frg, ack, typeField, rt, sequenceNumber, userId, destinationId,
                                dataLength)  # creating the joinRoomRequest packet
        self.transport.write(buf.raw)

    def sendLeaveSystemRequestOIE(self):
        """
        Called **by the controller**  when the user
        has clicked on the leave button in the main room.
        """

        # defining fields of the packet
        frg = 0b0
        ack = 0b0
        typeField = 0b1111
        rt = 0b00
        sequenceNumber = self.cssn
        userId = self.userId
        destinationId = 0

        # creating the leaveSystemRequest packet
        buf = pack.createPacket(frg, ack, typeField, rt, sequenceNumber, userId, destinationId, 0)
        self.transport.write(buf.raw)

    def dataReceived(self, data):
        """
        :param data: The message received from the server
        :type data: A string of indeterminate length

        Twisted calls this method whenever new data is received on this
        connection.
        """

        # framing
        if (self.buf == None):
            self.buf = data
        else:
            self.buf += data

        # adding bytes to the buffer until length of the buffer equals to 6
        while (len(self.buf) >= 6):

            # unpacking the dataLength from the buffer
            dataLength = struct.unpack_from('>H', self.buf, 4)

            if (len(self.buf) >= dataLength[0] + 6):
                datagram = self.buf[:dataLength[0] + 6]
                self.buf = self.buf[dataLength[0] + 6:]
            else:
                return

            self.treatPacket(datagram)

    def treatPacket(self, datagram):

        # unpacking the received datagram
        (firstByte, receivedSequenceNumber, userId, destinationId, datalength, data) = pack.unpackPacket(datagram)

        # loginRequestAck received
        if (firstByte == 0b01000011 and self.cssn == receivedSequenceNumber):
            self.userId = userId
            self.cssn += 1

        # movieList received
        elif (firstByte == 0b00001111 and self.sssn == receivedSequenceNumber):
            self.sssn += 1

            # creating an ACK for movieList
            buf = pack.createAck(0b0011, receivedSequenceNumber, self.userId, 0)
            self.transport.write(buf.raw)

            # unpacking the movieList
            (self.movieList, self.movieByName) = pack.unpackMovieList(datagram)

        # userList received
        elif (firstByte == 0b00010100 and self.sssn == receivedSequenceNumber):
            self.sssn += 1

            # creating an ACK for userList
            buf = pack.createAck(0b0101, receivedSequenceNumber, self.userId, 0)
            self.transport.write(buf.raw)

            # unpacking the userList
            (self.userList, self.userById) = pack.unpackUserList(datagram, self.movieName)

            if (receivedSequenceNumber == 1 and self.initialization != 1):
                self.clientProxy.initCompleteONE(self.userList, self.movieList)

                # GUI initialization
                self.initialization = 1
            else:
                # setting the new user list in GUI
                self.clientProxy.setUserListONE(self.userList)

        # roomRequestAck received
        elif (firstByte == 0b01100011 and self.cssn == receivedSequenceNumber):
            if (datalength != 0):
                (Ip, port) = pack.unpackRoomRequestAck(datagram)  # unpacking the IP address and port
                self.clientProxy.updateMovieAddressPort(self.movieName, Ip, port)  # updating the details in movieList
            self.clientProxy.joinRoomOKONE()  # changing the current room (join/leave)
            self.cssn += 1

        # messageAck received
        elif (firstByte == 0b01000111 and self.cssn == receivedSequenceNumber):
            self.cssn += 1

        # leaveRequestAck received
        elif (firstByte == 0b01111111 and self.cssn == receivedSequenceNumber):
            self.cssn += 1

            # leaving the system
            self.clientProxy.leaveSystemOKONE()

        # forwarded message received
        elif ((firstByte == 0b00110001 or firstByte == 0b00110000) and self.sssn == receivedSequenceNumber):
            self.sssn += 1

            # creating an ACK for forwarded message
            buf = pack.createAck(0b1100, receivedSequenceNumber, self.userId, 0)
            self.transport.write(buf.raw)

            # taking the sender userName from the dictionnary
            userName = self.userById[userId]

            # displaying the message
            self.clientProxy.chatMessageReceivedONE(userName, data)


        # error received
        elif (firstByte == 0b01111011 and self.cssn == receivedSequenceNumber):

            # unpacking the error
            data = struct.unpack('B', data)

            # checking the type of the error
            if (data[0] == 1):
                self.clientProxy.connectionRejectedONE("userName already used")
            elif (data[0] == 0b00000110):
                self.clientProxy.connectionRejectedONE("sequence Number incorrect")
