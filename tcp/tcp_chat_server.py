# -*- coding: utf-8 -*-
import ctypes
import logging
import struct

from c2w.main.constants import ROOM_IDS
from twisted.internet.protocol import Protocol
from utilities import pack

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.tcp_chat_server_protocol')


class c2wTcpChatServerProtocol(Protocol):
    def __init__(self, serverProxy, clientAddress, clientPort):

        """
        :param serverProxy: The serverProxy, which the protocol must use
            to interact with the user and movie store (i.e., the list of users
            and movies) in the server.
        :param clientAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param clientPort: The port number used by the c2w server,
            given by the user.

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: serverProxy

            The serverProxy, which the protocol must use
            to interact with the user and movie store in the server.

        .. attribute:: clientAddress

            The IP address (or the name) of the c2w server.

        .. attribute:: clientPort

            The port number used by the c2w server.

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.

        .. note::
            The IP address and port number of the client are provided
            only for the sake of completeness, you do not need to use
            them, as a TCP connection is already associated with only
            one client.
        """
        self.clientAddress = clientAddress
        self.clientPort = clientPort
        self.serverProxy = serverProxy

        self.sssn = 0  # server side sequence number, stored by the server
        self.cssn = 0  # client side sequence number, stored by the server

        self.userList = []

        self.buf = None

    def dataReceived(self, data):

        """
        :param data: The message received from the server
        :type data: A string of indeterminate length

        Twisted calls this method whenever new data is received on this
        connection.
        """

        # framing
        if (self.buf == None):
            self.buf = data
        else:
            self.buf += data

        # adding bytes to the buffer until length of the buffer equals to 6
        while (len(self.buf) >= 6):

            # unpacking the dataLength from the buffer
            dataLength = struct.unpack_from('>H', self.buf, 4)

            if (len(self.buf) >= (dataLength[0] + 6)):
                datagram = self.buf[:dataLength[0] + 6]
                self.buf = self.buf[dataLength[0] + 6:]
            else:
                return

            if (datagram != None):
                self.treatPacket(datagram)

    def treatPacket(self, datagram):

        (firstByte, receivedSequenceNumber, userId, destinationId, dataLength, data) = pack.unpackPacket(datagram)

        # login request received
        if (firstByte == 0b00000011 and receivedSequenceNumber == 0):

            # checking if name is already used
            if (self.serverProxy.userExists(data)):

                # sending the error about existing of the user with the same name
                self.sendError(receivedSequenceNumber, userId, destinationId,
                               0b01)
            else:
                # adding the new user to the system
                defUserId = self.serverProxy.addUser(data, ROOM_IDS.MAIN_ROOM, self)

                # creating the ACK for the loginRequest
                buf = pack.createAck(0b0000, self.sssn, defUserId, destinationId)
                self.transport.write(buf.raw)

                # sending the movieList
                self.sendMovieList(defUserId, self.sssn)
                self.cssn += 1

        elif (firstByte == 0b00000011 and receivedSequenceNumber != 0):
            # sending the error about incorrect sequence number
            self.sendError(0, userId, destinationId, 0b00000110)


        # movieListAck received
        elif (firstByte == 0b01001111 and self.sssn == receivedSequenceNumber):
            self.sssn += 1
            self.updateUserList()  # updating and sending the userList


        # userList ack received
        elif (firstByte == 0b01010111 and self.sssn == receivedSequenceNumber):
            self.sssn += 1


        # joinRoomRequest received
        elif (firstByte == 0b00100001):
            self.cssn += 1
            typeField = 0b1000

            if (destinationId != 0):
                # defining the IP address and port of the movie room
                (ip, port) = self.serverProxy.getMovieAddrPortById(destinationId)

                # creating the ACK for the joinRoomRequest for joining the movie room
                buf = pack.createRoomRequestAck(typeField, receivedSequenceNumber, userId, destinationId, 6, ip, port)
                self.transport.write(buf.raw)

                # searching for the movie title by using its ID
                movie = self.serverProxy.getMovieById(destinationId)
                movieTitle = movie.movieTitle

                # searching for the user name by using his ID
                user = self.serverProxy.getUserById(userId)
                userName = user.userName

                # updating the users current chat room
                self.serverProxy.updateUserChatroom(userName, movieTitle)

                # updating and sending the userList
                self.updateUserList()

                # starting the streaming video
                self.serverProxy.startStreamingMovie(movieTitle)

            else:
                # creating the joinRoomRequest for leaving the movie room
                buf = pack.createRoomRequestAck(typeField, receivedSequenceNumber, userId, destinationId, 0)
                self.transport.write(buf.raw)

                # searching the user name by using his ID
                user = self.serverProxy.getUserById(userId)
                userName = user.userName

                # updating the users current chat room
                self.serverProxy.updateUserChatroom(userName, ROOM_IDS.MAIN_ROOM)

                # updating and sending the userList
                self.updateUserList()


        # message received in the main or a movie room
        elif ((firstByte == 0b00000100 or firstByte == 0b00000101) and self.cssn == receivedSequenceNumber):
            self.cssn += 1
            typeField = 0b0001
            buf = pack.createAck(typeField, receivedSequenceNumber, userId, destinationId)  # creating the ACK for the message
            self.transport.write(buf.raw)

            user = self.serverProxy.getUserById(userId)  # checking the current user chat room
            userCurrentRoom = user.userChatRoom
            self.userList = self.serverProxy.getUserList()

            for i in self.userList:

                # looking for the users who are in the same chat room
                if (i.userChatRoom == userCurrentRoom and i.userId != userId):
                    userChatInstance = i.userChatInstance

                    # creating the messageForward packet
                    messageForward = self.packMessageForward(userChatInstance, userCurrentRoom, userId, i.userId,
                                                             len(data), data)

                    # forwarding the message to all the clients in the same chat room
                    userChatInstance.transport.write(messageForward.raw)
                    userChatInstance.sssn += 1


        # leave system request received
        elif (firstByte == 0b00111100 and self.cssn == receivedSequenceNumber):

            # creating the leaveSystemRequestAck
            typeField = 0b1111
            buf = pack.createAck(typeField, receivedSequenceNumber, userId, destinationId)
            self.transport.write(buf.raw)

            # checking who has sent the request
            user = self.serverProxy.getUserById(userId)
            userName = user.userName

            # deleting the user
            self.serverProxy.removeUser(userName)

            # updating and sending the userList
            self.updateUserList()


        # messageForwardedAck received
        elif (firstByte == 0b01110011 and self.sssn == receivedSequenceNumber):
            self.sssn += 1


        # errorAck received
        elif (firstByte == 0b01110001 and self.sssn == receivedSequenceNumber):
            self.sssn += 1

    def sendError(self, sequenceNumber, userId, destinationId, data):

        # defining fields of the packet
        frg = 0b0
        ack = 0b1
        typeField = 0b1110
        rt = 0b11
        dataLength = 1
        premierOctet = (frg << 7) | (ack << 6) | (typeField << 2) | (rt)
        buf = ctypes.create_string_buffer(6 + dataLength)
        userId = userId + 1
        struct.pack_into('>BBBBHB', buf, 0, premierOctet, sequenceNumber, userId, destinationId, dataLength, data)
        self.transport.write(buf.raw)

    def sendMovieList(self, userId, sequenceNumber):

        movieList = self.serverProxy.getMovieList()
        buf = pack.packMovieList(movieList, userId, sequenceNumber)
        self.transport.write(buf.raw)

    def sendUserList(self, userId, userList):

        user = self.serverProxy.getUserById(userId)
        userChatInstance = user.userChatInstance
        self.userList = userList
        sequenceNumber = userChatInstance.sssn
        buf = pack.packUserList(self.userList, userId, sequenceNumber)
        userChatInstance.transport.write(buf.raw)

    def updateUserList(self):

        userList = self.serverProxy.getUserList()
        for i in userList:
            self.sendUserList(i.userId, userList)

    def packMessageForward(self, userChatInstance, userChatRoom, senderUserId, destinationId, dataLength, data):

        if (userChatRoom == ROOM_IDS.MAIN_ROOM):
            rt = 0b00
        else:
            rt = 0b01

        buf = pack.createPacket(0b0, 0b0, 0b1100, rt, userChatInstance.sssn, senderUserId, destinationId, dataLength,
                                data)
        firstByte = struct.unpack_from('>B', buf, 0)
        return buf
