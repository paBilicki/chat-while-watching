# -*- coding: utf-8 -*-
import ctypes
import logging
import struct

from c2w.main.constants import ROOM_IDS
from c2w.main.lossy_transport import LossyTransport
from twisted.internet import reactor
from twisted.internet.protocol import DatagramProtocol
from utilities import pack

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.udp_chat_server_protocol')


class c2wUdpChatServerProtocol(DatagramProtocol):
    def __init__(self, serverProxy, lossPr):
        """
        :param serverProxy: The serverProxy, which the protocol must use
            to interact with the user and movie store (i.e., the list of users
            and movies) in the server.
        :param lossPr: The packet loss probability for outgoing packets.  Do
            not modify this value!

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attribute:

        .. attribute:: serverProxy

            The serverProxy, which the protocol must use
            to interact with the user and movie store in the server.

        .. attribute:: lossPr

            The packet loss probability for outgoing packets.  Do
            not modify this value!  (It is used by startProtocol.)

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """
        self.serverProxy = serverProxy
        self.lossPr = lossPr

        self.sssn = {}  # dictionnary linking the user to his server side sequence number, stored by the server - id is a key
        self.cssn = {}  # dictionnary linking the user to his client side sequence number, stored by the server - id is a key
        self.timer = {}  # dictionnary linking the user to his timer - id is a key
        self.ackReceived = {}  # dictionnary linking the user to his acknowledgement state - id is a key

        self.userList = []

    def startProtocol(self):
        """
        DO NOT MODIFY THE FIRST TWO LINES OF THIS METHOD!!
        """
        self.transport = LossyTransport(self.transport, self.lossPr)
        DatagramProtocol.transport = self.transport

    def datagramReceived(self, datagram, (host, port)):
        """
        :param string datagram: the payload of the UDP packet.
        :param host: the IP address of the source.
        :param port: the source port.

        Called **by Twisted** when the server has received a UDP
        packet.
        """
        address = (host, port)

        # unpacking the received datagram
        (firstByte, receivedSequenceNumber, userId, destinationId, dataLength, data) = pack.unpackPacket(datagram)

        # loginRequest received
        if (firstByte == 0b00000011 and receivedSequenceNumber == 0):

            # checking if name is already used
            if (self.serverProxy.userExists(data)):
                user = self.serverProxy.getUserByName(data)
                userAddress = user.userAddress

                # checking if last connected user received the ACK
                if (userAddress == address):

                    # synchronization the sequence numbers
                    self.sssn[user.userId] = 0
                    self.cssn[user.userId] = 0
                    self.ackReceived[user.userId] = 0
                    destId = 0

                    # creating the ACK for the loginRequest
                    buf = pack.createAck(0b0000, self.sssn[user.userId], user.userId, destId)
                    self.transport.write(buf.raw, address)

                else:
                    # sending the error about existing of the user with the same name
                    self.sendError(receivedSequenceNumber, userId, destinationId, 0b01,
                                   address)
            else:
                # adding the new user to the system
                defUserId = self.serverProxy.addUser(data, ROOM_IDS.MAIN_ROOM, None, address)

                self.sssn[defUserId] = 0
                self.cssn[defUserId] = 0
                self.ackReceived[defUserId] = 0
                destId = 0

                # creating the ACK for the loginRequest
                buf = pack.createAck(0b0000, self.sssn[defUserId], defUserId, destId)
                self.transport.write(buf.raw, address)

                # sending the movieList
                self.sendMovieList(address, defUserId, self.sssn[defUserId])
                self.cssn[defUserId] += 1

        # sending the error about incorrect sequence number
        elif (firstByte == 0b00000011 and receivedSequenceNumber != 0):
            self.sendError(0, userId, destinationId, 0b00000110, address)

        # movieListAck received
        elif (firstByte == 0b01001111 and self.sssn[userId] == receivedSequenceNumber):
            self.sssn[userId] += 1
            self.ackReceived[userId] = 1
            self.transmissionFunction(userId, address)
            self.timer[userId].cancel()

            # updating and sending the userList
            self.updateUserList()


        # userList ack received
        elif (firstByte == 0b01010111 and self.sssn[userId] == receivedSequenceNumber):
            self.sssn[userId] += 1
            self.ackReceived[userId] = 1
            self.transmissionFunction(userId, address)
            self.timer[userId].cancel()

        # joinRoomRequest received
        elif (firstByte == 0b00100001 and self.cssn[userId] == receivedSequenceNumber):
            self.cssn[userId] += 1
            typeField = 0b1000

            if (destinationId != 0):
                # defining the IP address and port of the movie room
                (ip, port) = self.serverProxy.getMovieAddrPortById(destinationId)

                # creating the ACK for the joinRoomRequest for joining the movie room
                buf = pack.createRoomRequestAck(typeField, receivedSequenceNumber, userId, destinationId, 6, ip, port)
                self.transport.write(buf.raw, address)

                # searching for the movie title by using its ID
                movie = self.serverProxy.getMovieById(destinationId)
                movieTitle = movie.movieTitle

                # searching for the user name by using his ID
                user = self.serverProxy.getUserById(userId)
                userName = user.userName

                # updating the users current chat room
                self.serverProxy.updateUserChatroom(userName, movieTitle)

                # updating and sending the userList
                self.updateUserList()

                # starting the streaming video
                self.serverProxy.startStreamingMovie(movieTitle)

            else:
                # creating the joinRoomRequest for leaving the movie room
                buf = pack.createRoomRequestAck(typeField, receivedSequenceNumber, userId, destinationId, 0)
                self.transport.write(buf.raw, address)

                # searching the user name by using his ID
                user = self.serverProxy.getUserById(userId)
                userName = user.userName

                # updating the users current chat room
                self.serverProxy.updateUserChatroom(userName, ROOM_IDS.MAIN_ROOM)

                # updating and sending the userList
                self.updateUserList()

        # message received in the main or a movie room
        elif ((firstByte == 0b00000100 or firstByte == 0b00000101) and self.cssn[userId] == receivedSequenceNumber):
            self.cssn[userId] += 1
            typeField = 0b0001

            # creating the ACK for the message
            buf = pack.createAck(typeField, receivedSequenceNumber, userId, destinationId)
            self.transport.write(buf.raw, address)

            # checking the current user chat room
            user = self.serverProxy.getUserById(userId)
            userCurrentRoom = user.userChatRoom
            self.userList = self.serverProxy.getUserList()

            for i in self.userList:
                # looking for the users who are in the same chat room
                if (i.userChatRoom == userCurrentRoom and i.userId != userId):
                    address = i.userAddress

                    # creating the messageForward packet
                    messageForward = self.packMessageForward(i.userChatRoom, userId, i.userId, dataLength, data)

                    # forwarding the message to all the clients in the same chat room
                    self.transmissionFunction(i.userId, address,messageForward)


        # messageForwardedAck received
        elif (firstByte == 0b01110011 and self.sssn[userId] == receivedSequenceNumber):
            self.sssn[userId] += 1
            self.ackReceived[userId] = 1
            self.transmissionFunction(userId, address)
            self.timer[userId].cancel()


        # leave system request received
        elif (firstByte == 0b00111100 and self.cssn[userId] == receivedSequenceNumber):
            typeField = 0b1111

            # creating the leaveSystemRequestAck
            buf = pack.createAck(typeField, receivedSequenceNumber, userId, destinationId)
            self.transport.write(buf, address)

            # checking who has sent the request
            user = self.serverProxy.getUserById(userId)
            userName = user.userName

            # deleting the user
            self.serverProxy.removeUser(userName)

            # updating and sending the userList
            self.updateUserList()


        # errorAck received
        elif (firstByte == 0b01110001 and self.sssn[userId] == receivedSequenceNumber):
            self.sssn[userId] += 1
            self.ackReceived[userId] = 1
            self.timer[userId].cancel()

    def sendError(self, sequenceNumber, userId, destinationId, data, address):

        # defining fields of the packet
        frg = 0b0
        ack = 0b1
        typeField = 0b1110
        rt = 0b11
        dataLength = 1
        firstByte = (frg << 7) | (ack << 6) | (typeField << 2) | (rt)
        buf = ctypes.create_string_buffer(6 + dataLength)
        userId = userId + 1

        struct.pack_into('>BBBBHB', buf, 0, firstByte, sequenceNumber, userId, destinationId, dataLength, data)
        self.transport.write(buf.raw, address)

    def sendMovieList(self, address, userId, sequenceNumber):

        movieList = self.serverProxy.getMovieList()
        buf = pack.packMovieList(movieList, userId, sequenceNumber)
        self.transmissionFunction(userId, address, buf)

    def sendUserList(self, address, userId, sequenceNumber, userlist):

        user = self.serverProxy.getUserById(userId)
        self.userList = userlist
        buf = pack.packUserList(self.userList, userId, sequenceNumber)
        self.transmissionFunction(userId, address, buf)

    def transmissionFunction(self, userId, address, buf=None, counter=0):

        # checking if ACK has been received
        if (self.ackReceived[userId] == 1):
            self.ackReceived[userId] = 0
            counter = 0
            return

        # checking if the retransmission has been done more than 10 times
        if (counter > 10):

            # removing the user who has not responded for 10 times
            self.serverProxy.removeUser(self.serverProxy.getUserById(userId).userName)

            # updating and sending the userList
            self.updateUserList()
            return

        self.transport.write(buf.raw, address)
        counter += 1
        self.timer[userId] = reactor.callLater(3, self.transmissionFunction, userId, address, buf, counter)

    def updateUserList(self):

        userList = self.serverProxy.getUserList()

        for i in userList:
            userAddress = i.userAddress
            self.sendUserList(userAddress, i.userId, self.sssn[i.userId], userList)

    def packMessageForward(self, userChatRoom, senderUserId, destinationId, dataLength, data):

        if (userChatRoom == ROOM_IDS.MAIN_ROOM):
            rt = 0b00
        else:
            rt = 0b01

        buf = pack.createPacket(0b0, 0b0, 0b1100, rt, self.sssn[destinationId], senderUserId, destinationId, dataLength,
                                data)
        return buf
