# -*- coding: utf-8 -*-
import logging
import struct

from c2w.main.constants import ROOM_IDS
from c2w.main.lossy_transport import LossyTransport
from twisted.internet import reactor
from twisted.internet.protocol import DatagramProtocol
from utilities import pack

logging.basicConfig()
moduleLogger = logging.getLogger('c2w.protocol.udp_chat_client_protocol')


class c2wUdpChatClientProtocol(DatagramProtocol):
    def __init__(self, serverAddress, serverPort, clientProxy, lossPr):
        """
        :param serverAddress: The IP address (or the name) of the c2w server,
            given by the user.
        :param serverPort: The port number used by the c2w server,
            given by the user.
        :param clientProxy: The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        Class implementing the UDP version of the client protocol.

        .. note::
            You must write the implementation of this class.

        Each instance must have at least the following attributes:

        .. attribute:: serverAddress

            The IP address (or the name) of the c2w server.

        .. attribute:: serverPort

            The port number used by the c2w server.

        .. attribute:: clientProxy

            The clientProxy, which the protocol must use
            to interact with the Graphical User Interface.

        .. attribute:: lossPr

            The packet loss probability for outgoing packets.  Do
            not modify this value!  (It is used by startProtocol.)

        .. note::
            You must add attributes and methods to this class in order
            to have a working and complete implementation of the c2w
            protocol.
        """

        self.serverAddress = serverAddress
        self.serverPort = serverPort
        self.clientProxy = clientProxy
        self.lossPr = lossPr

        self.userId = 0  # the ID of the user

        self.ackReceived = 0  # the variable used to check if acknowledgement was received
        self.timer = 0  # timer used by callLater function

        self.movieList = []
        self.userList = []

        self.movieName = ROOM_IDS.MAIN_ROOM

        self.cssn = 0  # client side sequence number stored by the client
        self.sssn = 0  # server side sequence number stored by the client
        self.initialization = 0  # variable which assures that initCompleteONE function is called only once

        self.movieByName = {}  # dictionnary linking the movieId to its title - title is a key
        self.userById = {}  # dictionnary linking the userName to his id - id is a key

    def startProtocol(self):
        """
        DO NOT MODIFY THE FIRST TWO LINES OF THIS METHOD!!
        """
        self.transport = LossyTransport(self.transport, self.lossPr)
        DatagramProtocol.transport = self.transport

    def sendLoginRequestOIE(self, userName):
        """
        :param string userName: The user name that the user has typed.

        The controller calls this function as soon as the user clicks on
        the login button.
        """

        moduleLogger.debug('loginRequest called with username=%s', userName)

        frg = 0b0  # defining different fields of the packet
        ack = 0b0
        typeField = 0b0000
        rt = 0b11
        sequenceNumber = 0b00000000
        dataLength = len(userName)
        userId = 0b00000000
        destinationId = 0b00000000

        # creating the loginRequest packet
        buf = pack.createPacket(frg, ack, typeField, rt, sequenceNumber, userId, destinationId, dataLength, userName)
        address = (self.serverAddress, self.serverPort)
        self.transmissionFunction(address, buf)

    def sendChatMessageOIE(self, message):
        """
        :param message: The text of the chat message.
        :type message: string

        Called **by the controller**  when the user has decided to send
        a chat message

        .. note::
           This is the only function handling chat messages, irrespective
           of the room where the user is.  Therefore it is up to the
           c2wChatClientProctocol or to the server to make sure that this
           message is handled properly, i.e., it is shown only by the
           client(s) who are in the same room.
        """

        # defining fields of the packet
        frg = 0b0
        ack = 0b0
        typeField = 0b0001
        sequenceNumber = self.cssn
        dataLength = len(message)

        # distinguishing between movie room and main room
        if (self.movieName == ROOM_IDS.MAIN_ROOM):
            destinationId = 0
            rt = 0b00
        else:
            destinationId = self.movieByName[self.movieName]
            rt = 0b01

        # creating the chatMessage packet
        buf = pack.createPacket(frg, ack, typeField, rt, sequenceNumber, self.userId, destinationId, dataLength,
                                message)

        address = (self.serverAddress, self.serverPort)
        self.transmissionFunction(address, buf)

    def sendJoinRoomRequestOIE(self, roomName):
        """
        :param roomName: The room name (or movie title.)

        Called **by the controller**  when the user
        has clicked on the watch button or the leave button,
        indicating that she/he wants to change room.

        .. warning:
            The controller sets roomName to
            c2w.main.constants.ROOM_IDS.MAIN_ROOM when the user
            wants to go back to the main room.
        """
        self.movieName = roomName

        # defining fields of the packet
        frg = 0b0
        ack = 0b0
        typeField = 0b1000
        rt = 0b01
        sequenceNumber = self.cssn
        dataLength = 0

        # distinguishing between join movie room and leave movie room
        if (roomName == ROOM_IDS.MAIN_ROOM):
            destinationId = 0

        else:
            destinationId = self.movieByName[roomName]

        # creating the joinRoomRequest packet
        buf = pack.createPacket(frg, ack, typeField, rt, sequenceNumber, self.userId, destinationId, dataLength)
        address = (self.serverAddress, self.serverPort)
        self.transmissionFunction(address, buf)

    def sendLeaveSystemRequestOIE(self):
        """
        Called **by the controller**  when the user
        has clicked on the leave button in the main room.
        """

        # defining fields of the packet
        frg = 0b0
        ack = 0b0
        typeField = 0b1111
        rt = 0b00
        sequenceNumber = self.cssn
        userId = self.userId
        destinationId = 0

        # creating the leaveSystemRequest packet
        buf = pack.createPacket(frg, ack, typeField, rt, sequenceNumber, userId, destinationId, 0)
        address = (self.serverAddress, self.serverPort)
        self.transport.write(buf, address)

    def datagramReceived(self, datagram, (host, port)):

        address = (host, port)

        # unpacking the received datagram
        (premierOctet, sequenceNumberRec, userId, destinationId, dataLength, data) = pack.unpackPacket(datagram)

        # loginRequestAck received
        if (premierOctet == 0b01000011 and self.cssn == sequenceNumberRec):
            self.userId = userId
            self.ackReceived = 1
            self.transmissionFunction(address)
            self.timer.cancel()
            self.cssn += 1

        # movieList received
        elif (premierOctet == 0b00001111 and self.sssn == sequenceNumberRec):
            self.sssn += 1

            # creating an ACK for movieList
            buf = pack.createAck(0b0011, sequenceNumberRec, self.userId, 0)
            self.transport.write(buf.raw, address)

            # unpacking the movieList
            (self.movieList, self.movieByName) = pack.unpackMovieList(datagram)

        # userList received
        elif (premierOctet == 0b00010100 and self.sssn == sequenceNumberRec):
            self.sssn += 1

            # creating an ACK for userList
            buf = pack.createAck(0b0101, sequenceNumberRec, self.userId, 0)
            self.transport.write(buf.raw, address)

            # unpacking the userList
            (self.userList, self.userById) = pack.unpackUserList(datagram, self.movieName)

            if (sequenceNumberRec == 1 and self.initialization != 1):

                # GUI initialization
                self.clientProxy.initCompleteONE(self.userList, self.movieList)
                self.initialization = 1
            else:

                # setting the new user list in GUI
                self.clientProxy.setUserListONE(self.userList)

        # roomRequestAck received
        elif (premierOctet == 0b01100011 and self.cssn == sequenceNumberRec):

            if (dataLength != 0):

                # unpacking the IP address and port
                (Ip, port) = pack.unpackRoomRequestAck(datagram)

                # updating the details in movieList
                self.clientProxy.updateMovieAddressPort(self.movieName, Ip, port)

            # changing the current room (join/leave)
            self.clientProxy.joinRoomOKONE()
            self.ackReceived = 1
            self.transmissionFunction(address)
            self.timer.cancel()
            self.cssn += 1

        # messageAck received
        elif (premierOctet == 0b01000111 and self.cssn == sequenceNumberRec):
            self.ackReceived = 1
            self.transmissionFunction(address)
            self.timer.cancel()
            self.cssn += 1

        # leaveRequestAck received
        elif (premierOctet == 0b01111111 and self.cssn == sequenceNumberRec):
            self.cssn += 1
            self.ackReceived = 1

            # leaving the system
            self.clientProxy.leaveSystemOKONE()

            # closing the application
            self.clientProxy.applicationQuit()

        # forwarded message received
        elif ((premierOctet == 0b00110001 or premierOctet == 0b00110000) and self.sssn == sequenceNumberRec):
            self.sssn += 1

            # creating an ACK for forwarded message
            buf = pack.createAck(0b1100, sequenceNumberRec, self.userId, 0)
            self.transport.write(buf.raw, address)

            # taking the sender userName from the dictionnary
            userName = self.userById[userId]

            # displaying the message
            self.clientProxy.chatMessageReceivedONE(userName, data)

        # error received
        elif (premierOctet == 0b01111011 and self.cssn == sequenceNumberRec):

            # unpacking the error
            data = struct.unpack('B', data)

            # checking the type of the error
            if (data[0] == 1):
                self.clientProxy.connectionRejectedONE("This user name is already used")
            elif (data[0] == 0b00000110):
                self.clientProxy.connectionRejectedONE("The sequence number is incorrect")

            self.ackReceived = 1
            self.transmissionFunction(address)
            self.timer.cancel()

    def transmissionFunction(self, address, buf=None, counter=0):

        # checking if ACK has been received
        if (self.ackReceived == 1):
            self.ackReceived = 0
            return

        # checking if the retransmission has been done more than 10 times
        if (counter > 10):
            return

        self.transport.write(buf.raw, address)
        counter += 1
        self.timer = reactor.callLater(3, self.transmissionFunction, address, buf, counter)
