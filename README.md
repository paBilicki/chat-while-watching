# Chat While Watching - 2014#

The goal for the project was to define and implement session layer protocol capable of supporting the simple video application. The idea of the application being that users can connect to a server offering several video streams, select a single video stream and chat with the other users watching the same stream. The protocol was capable of supporting all the communication requirements of the application, it had to define all required types of messages for various scenarios. The protocol was able to use either TCP or UDP. 

### Specifications ###
In the specs directory there are two specifications:

* **conceived_spec-r302-s14-g19.pdf** - worked out by the group during the 1 stage of the project

* **applied_spec_r302-s14-g9.pdf** - implemented during the 2nd phase of the project